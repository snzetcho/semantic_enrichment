from django.contrib import admin
from django.conf.urls import url, include
# from queries.models import Query
from rest_framework import routers, serializers, viewsets

from . import views

# class QuerySerializer(serializers.HyperlinkedModelSerializer):
#     class Meta:
#         model = Query
#         fields = ('name', 'query_content')

# class QueryViewSet(viewsets.ModelViewSet):
#     queryset = Query.objects.all()
#     serializer_class = QuerySerializer

# # Routers provide an easy way of automatically determining the URL conf
# router = routers.DefaultRouter()
# router.register(r'queries', QueryViewSet)


urlpatterns = [
    # ex: /polls/
    url(r'^$', views.index, name='index'),
    # ex: /polls/5/
    url(r'^retrieveQuery/$', views.retrieveQuery, name='retrieveQuery'),
    url(r'^executeQuery/$', views.executeQuery, name='executeQuery'),
    url(r'^changeOPType/$', views.changeOPType, name = 'changeOPType'),
    url(r'^addStrategyToTwo5D/$', views.addStrategyToTwo5D, name = 'addStrategyToTwo5D'),
    # url(r'^', include(router.urls)),
    # url(r'^api-auth/', include('rest_framework.urls'))
]
