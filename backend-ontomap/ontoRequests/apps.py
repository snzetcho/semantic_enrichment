from django.apps import AppConfig


class OntorequestsConfig(AppConfig):
    name = 'ontoRequests'
