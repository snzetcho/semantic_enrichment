from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, HttpResponse

from django.template import loader

from application.settings import conn

from src.coreFunc import makeListItem, runUpdateRequest, getVariables


import src.displayGraphs as dg
import urllib, base64
# from coreFunc import *

def index(request):
    # chooseFrom(['a', 'b', 'c', 'd', 'e', 'f', 'g'])
    # template = loader.get_template('ontoRequests/home.html')
    return render(request, 'ontoRequests/home.html', {})

def retrieveQuery(request):
    context = {'visibility' : request.POST['visib']}
    # print(request.POST)
    # return HttpResponse('La valeur de visibilité est ' + context['visibility'])
    # template = loader.get_template('ontoRequests/fromCong.temp.html')
    return render(request, 'ontoRequests/fromCong.temp.html', context)
    # return HttpResponse(template.render(context, request))

def executeQuery(request):
    global conn
    myReq = ''
    if request.POST['graph'] == 'yes' and len(getVariables(request.POST['query']))==2 :
        myReq = request.POST['query']
        myList = makeListItem(conn, myReq)
        keys = getVariables(myReq)
        context = {'isGraph' : True }
        try:
            print('1')
            graph = dg.makeGraphFromList(keys, myList)
            print('2')
            string = urllib.parse.quote(base64.b64encode(graph.read()))
            print('3')
            context['graph'] = string
            print('4')
            string.close()
            print('5')
        except:
            pass
        return render(request, 'ontoRequests/result.html', context)
    else:
        try:
            myReq = """SELECT ?docAddress
            { ?wkstp owl:Class stp:machining_workingstep .
             ?mop owl:Class stp:machining_operation .
             ?myTool owl:Class stp:machining_tool .
             ?docRef owl:Class stp:document .
             ?wkstp stp:machining_workingstep_has_operation ?mop .
             ?mop stp:machining_operation_has_tool ?myTool .
             ?myTool stp:machining_tool_has_id ?o .
             ?wkstp stp:element_has_document_reference ?docRef .
             ?docRef stp:document_has_name ?docAddress
            filter(?o = "%s"^^<http://www.w3.org/2001/XMLSchema#string>)
            }"""
            print(request.POST['toolID'])
            myReq = myReq%(request.POST['toolID'])
            context = {'isFinal' : True }
        except:
            myReq = request.POST['query']
            print('myReq: ', myReq)
            context = {'isFinal' : False }
        myList = makeListItem(conn, myReq)
        varsName = getVariables(myReq)
        context['isGraph'] = False
        context['varsName'] = varsName
        context['itemList'] = myList
        return render(request, 'ontoRequests/result.html', context)

def changeOPType(request):
    global conn
    OntoSTEPNC = "http://www.semanticweb.org/ontologies/OntoSTEP-NC#"
    conn.setNamespace('OntoSTEPNC', OntoSTEPNC)
    with open('mats/annotationsMOP.sparql') as filHandler:
        myReqs = filHandler.read().split('##')
        # 0 : Get all op / tool where op is of undefined type ("stp:machining_operation")
        # 1 : Get all possible kind of OP in STEP-NC (bar rough/finish distingo)
        # 2 : Update the base (1st arg : new OP_type ; 2nd arg : targetted op)
        # 3 : Get all mop of type two5D_milling_operation or sub classes
        # 4 : Get all possible kind of two5D_milling_strat
        # 5 : Insert the strategy descriptor
    try:
        a = request.POST['ChangeType'] != ''
    except:
        a = False
    if a :
        for key in request.POST:
            if "op_type" in key:
                if request.POST[key] != 'SKIP':

                    reqTmp = myReqs[2]%(conn.createURI(namespace = OntoSTEPNC, localname = request.POST[key].replace('op_type.', '')),
                                        conn.createURI(namespace = OntoSTEPNC, localname = key.replace('op_type.', '')))
                    print(runUpdateRequest(conn, reqTmp))
        pass  # Opérer le changement en base
    itemList = makeListItem(conn, myReqs[0])
    context = {'itemList' : itemList}
    context['classList'] = makeListItem(conn, myReqs[1])
    return render(request, 'ontoRequests/annotations_operations.html', context)

def addStrategyToTwo5D(request):
    global conn
    global conn
    OntoSTEPNC = "http://www.semanticweb.org/ontologies/OntoSTEP-NC#"
    conn.setNamespace('OntoSTEPNC', OntoSTEPNC)
    with open('mats/annotationsMOP.sparql') as filHandler:
        myReqs = filHandler.read().split('##')
        # 0 : Get all op / tool where op is of undefined type ("stp:machining_operation")
        # 1 : Get all possible kind of OP in STEP-NC (bar rough/finish distingo)
        # 2 : Update the base (1st arg : new OP_type ; 2nd arg : targetted op)
        # 3 : Get all mop of type two5D_milling_operation or sub classes
        # 4 : Get all possible kind of two5D_milling_strat
        # 5 : Insert the strategy descriptor
    try:
        a = request.POST['AddStrategy'] != ''
    except:
        a = False
    if a:
        for key in request.POST:
            if "strat" in key:
                if request.POST[key] != 'SKIP' :
                    reqTmp = myReqs[5]%(conn.createURI(namespace = OntoSTEPNC, localname = request.POST[key].replace('strat.', '')),
                                        conn.createURI(namespace = OntoSTEPNC, localname = key.replace('strat.', '')))
                    print(runUpdateRequest(conn, reqTmp))
                    # Do de dings
        pass
    itemList = makeListItem(conn, myReqs[3])
    myItems = []
    for i in itemList:
        try:
            graph = dg.makeGraphFromAdress(i[1], ['X', 'Y', 'Z'])
            string = urllib.parse.quote(base64.b64encode(graph.read()))
            myItems.append([i[0], string])
            string.close()
        except:
            pass
    context = {'itemList' : myItems}
    context['stratList'] = makeListItem(conn, myReqs[4])
    # Display trajectory along the name and possible strategies
    return render(request, 'ontoRequests/op_strategy.html', context)
