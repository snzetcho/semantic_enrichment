from django.shortcuts import render
from .models import Query
from rest_framework import routers, serializers, viewsets

from django.forms.models import model_to_dict
from django.http import JsonResponse
from src.coreFunc import makeListItem, runUpdateRequest, getVariables

from application.settings import conn
import src.displayGraphs as dg
import urllib, base64

# Create your views here.
class QuerySerializer(serializers.HyperlinkedModelSerializer):
  class Meta:
    model = Query
    fields = ('name', 'query_content')

class QueryViewSet(viewsets.ModelViewSet):
  queryset = Query.objects.all()
  serializer_class = QuerySerializer

def get_queries(request):
  if request.method != 'GET':
    pass
  else:
    queries = Query.objects.all()
    return JsonResponse({
          'status': 'success',
          'saved_queries': objects_to_array(queries)
      })

def post_query(request):
  try:
    name = request.POST.get('name', '')
    query_content = request.POST.get('query_content', '')
    qrs = Query.objects.filter(name=name)
    if (len(qrs) != 0):
      qrs[0].query_content = query_content
      qrs[0].save()
      return JsonResponse({
          'status': 'update'
      })
    else:
      qr = Query(name=name, query_content=query_content)
      qr.save()
      return JsonResponse({
          'status': 'add',
          'saved_query': model_to_dict(qr)
      })

  except Exception as e:
    print('error: ', e)
    return JsonResponse({
        'status': 'error'
    })

def delete_query(request, query_id):
  try:
    Query.objects.filter(id=query_id).delete()
    return JsonResponse({
        'status': 'delete query successfully'
    })
  except Exception as e:
    print('error: ', e)
    return JsonResponse({
        'status': 'error',
        'error': e
    })

def execute_sparql(request):
  global conn
  try:
    sparql_qr = request.POST.get('sparql_qr', '')
    print('sparql queries: ', sparql_qr)
    varValues = makeListItem(conn, sparql_qr)
    varNames = getVariables(sparql_qr)
    graph_str = ''
    if len(varNames) == 2 and len(varValues) > 0:
      print('get graph from list: ')
      graph = dg.makeGraphFromList(varNames, varValues)
      graph_str = urllib.parse.quote(base64.b64encode(graph.read()))

    print('varsName: ', varNames)
    return JsonResponse({
        'status': 'execute query successfully',
        'results': {
          'dataList': {
              'varNames': varNames,
              'varValues': varValues
            },
          'dataGraph': { 'graph_str': graph_str }
        }
    })

  except Exception as e:
    pass

def objects_to_array(objects):
  arr = []
  for object in objects:
    object_dict = model_to_dict(object)
    arr.append(object_dict)
  return arr