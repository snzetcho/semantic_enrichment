from django.conf.urls import url, include
from rest_framework import routers, serializers, viewsets
from . import views

urlpatterns = [
    # url(r'^queries', views.QueryViewSet.as_view(), name='queries api'),
    url(r'^$', views.get_queries, name='get queries'),
    url(r'^post$', views.post_query, name='post query'),
    url(r'^delete/(?P<query_id>[a-zA-Z0-9]+)/$', views.delete_query, name='delete query'),
    url(r'^execute-sparql$', views.execute_sparql, name='execute sparql query'),
    url(r'^api-auth/', include('rest_framework.urls'))
]