from franz.openrdf.vocabulary.owl import OWL
from franz.openrdf.vocabulary.xmlschema import XMLSchema

### TIPS GESTION CONTEXTE : ###
#     context1 = conn.createURI(namespace=exns, localname="context1")
#     context2 = conn.createURI(namespace=exns, localname="context2")
#     ## Assemble new statements and add them to the contexts.
#     conn.add(alice, RDF.TYPE, person, context1)
#     conn.add(alice, name, alicesName, context1)
#     conn.add(bob, RDF.TYPE, person, context2)
#     conn.add(bob, name, bobsName, context2)



def addNewProject(conn, projectName, addInf = {}, CompTag = None):
    OntoSTEPNC = "http://www.semanticweb.org/ontologies/OntoSTEP-NC#"
    conn.setNamespace('OntoSTEPNC', OntoSTEPNC)

    if CompTag :
        CON = conn.createURI(namespace = OntoSTEPNC,
                             localname = CompTag)
    else:
        CON = conn.createURI(namespace = OntoSTEPNC,
                             localname = "UTC")

    project = conn.createURI(namespace = OntoSTEPNC,
                             localname = "project")
    projectURI = conn.createURI(namespace = OntoSTEPNC,
                                localname = projectName)
    project_has_id = conn.createURI(namespace = OntoSTEPNC,
                                    localname = "project_has_id")
    projectLabel = conn.createLiteral(projectName,
                                      datatype = XMLSchema.STRING)

    if not conn.getStatements(projectURI, None, None):
        conn.addTriple(projectURI, OWL.CLASS, project, contexts = CON)
        conn.addTriple(projectURI, project_has_id, projectLabel, contexts = CON)

    workpiece = conn.createURI(namespace = OntoSTEPNC,
                               localname = "workpiece")
    workpieceURI = conn.createURI(namespace = OntoSTEPNC,
                                  localname = "part_" + projectName)
    project_has_workpiece = conn.createURI(namespace = OntoSTEPNC,
                                           localname = "project_has_workpiece")

    conn.addTriple(workpieceURI, OWL.CLASS, workpiece, contexts = CON)
    conn.addTriple(projectURI, project_has_workpiece, workpieceURI, contexts = CON)

    if "material" in addInf.keys():
        material = conn.createURI(namespace = OntoSTEPNC,
                                  localname = "material")
        materialURI = conn.createURI(namespace = OntoSTEPNC,
                                     localname = addInf['material'])
        workpiece_has_material = conn.createURI(namespace = OntoSTEPNC,
                                                localname = "workpiece_has_material")
        if not conn.getStatements(materialURI, None, None):
            material_has_id = conn.createURI(namespace = OntoSTEPNC,
                                             localname = 'material_has_material_identifier')
            matID = conn.createLiteral(addInf['material'],
                                      datatype = XMLSchema.STRING)
            conn.addTriple(materialURI, OWL.CLASS, material, contexts = CON)
            conn.addTriple(materialURI, material_has_id, matID, contexts = CON)
        conn.addTriple(workpieceURI, workpiece_has_material, materialURI, contexts = CON)

    if "owner" in addInf.keys():
        ownerURI = conn.createURI(namespace = OntoSTEPNC,
                                  localname = 'entity' + addInf['owner'])
        person = conn.createURI(namespace = OntoSTEPNC,
                                localname = addInf['owner'])
        personID = conn.createLiteral(addInf['owner'],
                                      datatype = XMLSchema.STRING)
    else:
        ownerURI = conn.createURI(namespace = OntoSTEPNC,
                                  localname = 'entity' + CompTag + '_generic')
        person = conn.createURI(namespace = OntoSTEPNC,
                                localname = CompTag + '_generic')
        personID = conn.createLiteral(CompTag + '_generic',
                                      datatype = XMLSchema.STRING)

    if not conn.getStatements(ownerURI, None, None):
        person_has_person = conn.createURI(namespace = OntoSTEPNC,
                                                       localname = 'person_and_address_has_person')
        person_and_address = conn.createURI(namespace = OntoSTEPNC,
                                            localname = 'person_and_adress')
        personClass = conn.createURI(namespace = OntoSTEPNC,
                                     localname = 'person')
        person_has_ID = conn.createURI(namespace = OntoSTEPNC,
                                       localname = 'person_has_name')
        conn.addTriple(ownerURI, OWL.CLASS, person_and_address, contexts = CON)
        conn.addTriple(person, OWL.CLASS, personClass, contexts = CON)
        conn.addTriple(ownerURI, person_has_person, person, contexts = CON)
        conn.addTriple(person, person_has_ID, personID, contexts = CON)

    project_has_owner = conn.createURI(namespace = OntoSTEPNC,
                                       localname = 'project_has_owner')
    conn.addTriple(projectURI, project_has_owner, ownerURI, contexts = CON)

    return projectURI


def addNewWorkplan(conn, workplanName, project, CompTag = None):
    OntoSTEPNC = "http://www.semanticweb.org/ontologies/OntoSTEP-NC#"
    conn.setNamespace('OntoSTEPNC', OntoSTEPNC)

    if CompTag :
        CON = conn.createURI(namespace = OntoSTEPNC,
                             localname = CompTag)
    else:
        CON = conn.createURI(namespace = OntoSTEPNC,
                             localname = "UTC")

    projectWkPlan = conn.createURI(namespace = OntoSTEPNC,
                                   localname = "project_has_main_workplan")

    wkPlan = conn.createURI(namespace = OntoSTEPNC,
                            localname = "workplan")
    wkPlanURI = conn.createURI(namespace = OntoSTEPNC,
                               localname = "wkPlan" + workplanName)

    conn.addTriple(wkPlanURI, OWL.CLASS, wkPlan, contexts = CON)
    conn.addTriple(project, projectWkPlan, wkPlanURI, contexts = CON)

    return wkPlanURI


def addCSVtoOnto(dictParam, workplan, conn, CompTag = None):
    dictURI = {}

    print('File : {0}'.format(dictParam['address']))

    OntoSTEPNC = "http://www.semanticweb.org/ontologies/OntoSTEP-NC#"
    conn.setNamespace('OntoSTEPNC', OntoSTEPNC)

    if CompTag :
        CON = conn.createURI(namespace = OntoSTEPNC,
                             localname = CompTag)
    else:
        CON = conn.createURI(namespace = OntoSTEPNC,
                             localname = "UTC")

    wkPlanElem = conn.createURI(namespace = OntoSTEPNC,
                                localname = "workplan_has_elements")

    machWkstp = conn.createURI(namespace = OntoSTEPNC,
                               localname = "machining_workingstep")
    dictURI[machWkstp] = conn.createURI(namespace = OntoSTEPNC,
                                        localname = "mWkStp" + dictParam['opName'])
    cnt = 0
    while conn.getStatements(dictURI[machWkstp], None, machWkstp):
        dictURI[machWkstp] = conn.createURI(namespace = OntoSTEPNC,
                                            localname = "mWkStp" + dictParam['opName'] + str(cnt))
        cnt += 1

    element_reference = conn.createURI(namespace = OntoSTEPNC,
                                       localname = "element_has_document_reference")
    document = conn.createURI(namespace = OntoSTEPNC,
                              localname = "document")
    dictURI[document] = conn.createURI(namespace = OntoSTEPNC,
                                       localname = "doc" + dictParam['address'].split('/')[-1].split('.')[0])
    documentType = conn.createURI(namespace = OntoSTEPNC,
                                  localname = "document_type")
    docHasType = conn.createURI(namespace = OntoSTEPNC,
                                localname = 'document_has_kind')
    dictURI[documentType] = conn.createURI(namespace = OntoSTEPNC,
                                           localname = "docTypeCSV")
    docTypeHasPDtype = conn.createURI(namespace = OntoSTEPNC,
                                      localname = "document_type_has_product_data_type")
    dictURI[dictURI[documentType]] = conn.createLiteral('csv',
                                                        datatype = XMLSchema.STRING)
    docHasID = conn.createURI(namespace = OntoSTEPNC,
                              localname = 'document_has_name')
    dictURI[docHasID] = conn.createLiteral(dictParam['address'],
                                           datatype = XMLSchema.STRING)

    machWkstpOP = conn.createURI(namespace = OntoSTEPNC,
                                 localname = "machining_workingstep_has_operation")

    machOP = conn.createURI(namespace = OntoSTEPNC,
                            localname = "machining_operation")
    dictURI[machOP] = conn.createURI(namespace = OntoSTEPNC,
                                     localname = "mOP" + dictParam['opName'])
    cnt = 0
    while conn.getStatements(dictURI[machOP], None, machOP):
        dictURI[machOP] = conn.createURI(namespace = OntoSTEPNC,
                                         localname = "mOP" + dictParam['opName'] + str(cnt))
        cnt += 1

    machOPtech = conn.createURI(namespace = OntoSTEPNC,
                                localname = "machining_operation_has_technology")

    tech = conn.createURI(namespace = OntoSTEPNC,
                          localname = "milling_technology")
    dictURI[tech] = conn.createURI(namespace = OntoSTEPNC,
                                   localname = "tech" + str(dictParam['feedRate']).split('.')[0] +
                                               str(dictParam['spindle']).split('.')[0])

    techSpindle = conn.createURI(namespace = OntoSTEPNC,
                                 localname = "technology_has_spindle_speed")
    dictURI[techSpindle] = conn.createLiteral(dictParam['spindle'],
                                              datatype = XMLSchema.FLOAT)
    techFeedrate = conn.createURI(namespace = OntoSTEPNC,
                                  localname = "technology_has_feedrate")

    speed = conn.createURI(namespace = OntoSTEPNC,
                           localname = "speed_measure")
    dictURI[speed] = conn.createLiteral(dictParam['feedRate'],
                                        datatype = XMLSchema.FLOAT)

    machOPtool = conn.createURI(namespace = OntoSTEPNC,
                                localname = "machining_operation_has_tool")

    machTool = conn.createURI(namespace = OntoSTEPNC,
                              localname = "machining_tool")
    dictURI[machTool] = conn.createURI(namespace = OntoSTEPNC,
                                       localname = "tool" + dictParam['toolRef'])

    label = conn.createURI(namespace = OntoSTEPNC, localname = "label")
    dictURI[label] = conn.createLiteral(dictParam['toolRef'], datatype = XMLSchema.STRING)

    machToolLabel = conn.createURI(namespace = OntoSTEPNC, localname = "machining_tool_has_id")

    # project = conn.createURI(namespace = OntoSTEPNC, localname = "label")

    conn.addTriple(dictURI[machWkstp], OWL.CLASS, machWkstp, contexts = CON)
    conn.addTriple(workplan, wkPlanElem, dictURI[machWkstp], contexts = CON)

    conn.addTriple(dictURI[machOP], OWL.CLASS, machOP, contexts = CON)
    conn.addTriple(dictURI[machWkstp], machWkstpOP, dictURI[machOP], contexts = CON)

    if 'previous' in dictParam.keys():
        precedence = conn.createURI(namespace = OntoSTEPNC,
                                    localname = "executable_has_antecedent")
        dictURI[precedence] = dictParam['previous']
        conn.addTriple(dictURI[machWkstp], precedence, dictURI[precedence], contexts = CON)

    if not conn.getStatements(dictURI[tech], None, None):
        conn.addTriple(dictURI[tech], OWL.CLASS, tech, contexts = CON)
        conn.addTriple(dictURI[machOP], machOPtech, dictURI[tech], contexts = CON)
        conn.addTriple(dictURI[speed], OWL.CLASS, speed, contexts = CON)
        conn.addTriple(dictURI[tech], techFeedrate, dictURI[speed], contexts = CON)
        conn.addTriple(dictURI[techSpindle], OWL.CLASS, speed, contexts = CON)
        conn.addTriple(dictURI[tech], techSpindle, dictURI[techSpindle], contexts = CON)
    else:
        conn.addTriple(dictURI[tech], techSpindle, dictURI[techSpindle], contexts = CON)
        conn.addTriple(dictURI[tech], techFeedrate, dictURI[speed], contexts = CON)
        conn.addTriple(dictURI[machOP], machOPtech, dictURI[tech], contexts = CON)

    if not conn.getStatements(dictURI[machTool], None, None):
        conn.addTriple(dictURI[machTool], OWL.CLASS, machTool, contexts = CON)
        conn.addTriple(dictURI[label], OWL.CLASS, label, contexts = CON)
    conn.addTriple(dictURI[machOP], machOPtool, dictURI[machTool], contexts = CON)
    conn.addTriple(dictURI[machTool], machToolLabel, dictURI[label], contexts = CON)

    conn.addTriple(dictURI[document], OWL.CLASS, document, contexts = CON)
    conn.addTriple(dictURI[machWkstp], element_reference, dictURI[document], contexts = CON)
    conn.addTriple(dictURI[documentType], OWL.CLASS, documentType, contexts = CON)
    conn.addTriple(dictURI[document], docHasType, dictURI[documentType], contexts = CON)
    conn.addTriple(dictURI[documentType], docTypeHasPDtype, dictURI[dictURI[documentType]], contexts = CON)
    conn.addTriple(dictURI[document], docHasID, dictURI[docHasID], contexts = CON)

    return dictURI[machWkstp]
