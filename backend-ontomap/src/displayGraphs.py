import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from mpl_toolkits.mplot3d import Axes3D
import io

from cycler import cycler

def barDisplay(inList):
    valueList = [[],[]]
    for i in inList:
        if not i in valueList[0]:
            valueList[0].append(i)
            valueList[1].append(1)
        else:
            valueList[1][valueList[0].index(i)] += 1
    plt.bar(range(len(valueList[0])), valueList[1], tick_label = valueList[0])
    plt.show()

def scatterDisplay(myDict, outputVar, annotation = False):
    labelList=[]
    for i in myDict.keys():
        labelList.append(i)
    if len(outputVar)==1:
        plt.xlabel('Tool')
        plt.ylabel(str(outputVar[0]))
        for i in myDict.keys():
            x_val = labelList.index(i)
            for j in myDict[i]:
                valueList[0].append(x_val)
                valueList[1].append(float(j))
                if annotation:
                    pass # TODO : Add a tool label on x axis
        plt.scatter(valueList[0], valueList[1])
    elif len(outputVar)==2:
        myMarkers = 'ov^<>8sp*hHDdPX'
        myColours = 'rgbycmk'
        cnt = 0
        for i in myDict.keys():
            valueList = [[],[]]
            for j in myDict[i]:
                valueList[0].append(j[0])
                valueList[1].append(j[1])
                if annotation and len(myDict.keys()) >= 1:
                    print("Expect label {0:<25} at position {1}".format(i, j))
                    plt.text(j[0], j[1], str(i))
                elif annotation :
                    pass
            plt.scatter(valueList[0], valueList[1], marker = myMarkers[cnt%15], c = myColours[cnt%7])
            cnt += 1
    plt.show()

# columns name : ['NumBrut', 'NumOutil', 'Ligne_programme', 'Cycle_usinage', 'Temps_absolu', 'Temps_relatif', 'Vc', 'Fz', 'h', 'AE', 'AP', 'AD', 'Diam_usine', 'Volume', 'Angle', 'Q', 'Temps_outil', 'OP', 'X','Y', 'Z', 'A', 'B', 'C', 'X.1', 'Y.1', 'Z.1', 'I', 'J', 'K', 'S', 'TypeIntersect', 'TypeMovement', 'TypeInOut', 'ToolRef', 'Tool1', 'Tool2', 'Dc', 'Lc', 'Rb', 'F', 'Void_41', 'Void_42', 'Void_43', 'Void_44', 'Void_45', 'prec_i_record', 'prec_i_passant', 'ind_i_record', 'ind_i_passant']

def displayFromCSVList(csvList, title, toDisplay):
    myMarkers = 'v<'
    myMarkers2 = '^>'
    myMarkersTotal = '^v<>8phH*.,o1234sP+xXDd'
    myColours = 'rgbycmk'
    cnt = 0
    timeScale = False
    if len(toDisplay) == 2:
        if timeScale:
            plt.xlabel('time')
            tmpStr = toDisplay[0] + ' ' + toDisplay[1]
            plt.ylabel(tmpStr)
        else:
            plt.xlabel(toDisplay[0])
            plt.ylabel(toDisplay[1])
        mxAP = 0
        mxAE = 0
        for i in csvList:
            # print(i)
            try:
                csvFile = pd.read_csv(i, sep = ';', encoding='ANSI')
            except:
                print("File not found : {}\n".format(i))
                break
            # with pd.read_csv(i, sep = ';', encoding='ANSI') as csvFile :
            ### csvFile is a pd.DataFrame
            ### read_csv gère l'ouverture et la fermeture du file handler
            # print("{} : {}".format(csvFile['AE'], csvFile['AP']))
            if max(csvFile[toDisplay[0]]) > mxAP:
                mxAP = max(csvFile[toDisplay[0]])
            if max(csvFile[toDisplay[1]]) > mxAE:
                mxAE = max(csvFile[toDisplay[1]])
            if timeScale :
                plt.scatter(csvFile['Temps_absolu'], csvFile.loc[csvFile['TypeInOut'] == 1][toDisplay[1]], marker = myMarkers[cnt%2], c = myColours[cnt%7], label = i.split('/')[-1] + str(toDisplay[1]))
                plt.scatter(csvFile['Temps_absolu'], csvFile.loc[csvFile['TypeInOut'] == 1][toDisplay[0]], marker = myMarkers2[cnt%2], c = myColours[cnt%7], label = i.split('/')[-1] + str(toDisplay[0]))
            else:
                plt.scatter(csvFile[toDisplay[0]], csvFile[toDisplay[1]], marker = myMarkersTotal[cnt%23], c = myColours[cnt%7])
            # print(csvFile.loc[csvFile['TypeInOut'] == 1]['AE'])
            cnt += 1
        plt.title(title)
        plt.xlim(xmin = -1 * mxAP*0.02, xmax = mxAP*1.1)
        plt.ylim(ymin = -1 * mxAE*0.02, ymax = mxAE*1.1)

        ############################################
        ### Pour impression en batch des graphes ###
        ############################################
        # plt.legend(loc = 0)
        # plt.savefig('X:/Google Drive/UTC_encrypted/Recherche/2-Productions/LUCID - Presentations/Réunion3101/CapturesAPAE/' + title + '.png')#, bbox_inches='tight')
        # plt.clf()

        plt.show()
    elif len(toDisplay) == 3:
        # print(csvList)
        for i in csvList:
            try:
                csvFile = pd.read_csv(i, sep = ';', encoding='ANSI')
            except:
                print("File not found : {}\n".format(i))
                break
            fig = plt.figure()
            ax = fig.add_subplot(111, projection = '3d')
            ax.set_xlabel(toDisplay[0])
            ax.set_ylabel(toDisplay[1])
            ax.set_zlabel(toDisplay[2])
            ax.plot(csvFile[toDisplay[0]], csvFile[toDisplay[1]], csvFile[toDisplay[2]])
            plt.show()
            plt.close(fig)

def makeGraphFromAdress(csvAdress, toDisplay):
    try:
        csvFile = pd.read_csv(csvAdress, sep = ';', encoding = 'ANSI')
    except:
        print("File not found : {}\n".format(i))
        return io.BytesIO()
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.set_xlabel(toDisplay[0])
    ax.set_ylabel(toDisplay[1])
    ax.set_zlabel(toDisplay[2])
    ax.plot(csvFile[toDisplay[0]], csvFile[toDisplay[1]], csvFile[toDisplay[2]])

    buf = io.BytesIO()
    fig.savefig(buf, format='png')
    buf.seek(0)
    plt.close()
    return buf

def makeGraphFromList(keys, myList):
    swapList = [[], []]
    xmax = 0
    ymax = 0
    for i in myList:
        if len(i):
            swapList[0].append(float(i[0]))
            if float(i[0]) > xmax :
                xmax = float(i[0])
            swapList[1].append(float(i[1]))
            if float(i[1]) > ymax :
                ymax = float(i[1])
    plt.xlabel(keys[0])
    plt.xlim(0, xmax*1.2)
    plt.ylabel(keys[1])
    plt.ylim(0, ymax*1.2)

    plt.scatter(swapList[0], swapList[1])
    buf = io.BytesIO()
    plt.savefig(buf, format = 'png')
    buf.seek(0)
    plt.close()
    return buf