import tempfile

from franz.openrdf.sail.allegrographserver import AllegroGraphServer
from franz.openrdf.repository.repository import Repository
from franz.miniclient import repository
from franz.openrdf.query.query import QueryLanguage
from franz.openrdf.model import URI
from franz.openrdf.vocabulary.rdf import RDF
from franz.openrdf.vocabulary.rdfs import RDFS
from franz.openrdf.vocabulary.owl import OWL
from franz.openrdf.vocabulary.xmlschema import XMLSchema
from franz.openrdf.query.dataset import Dataset
from franz.openrdf.rio.rdfformat import RDFFormat
from franz.openrdf.rio.rdfwriter import  NTriplesWriter
from franz.openrdf.rio.rdfxmlwriter import RDFXMLWriter

import src.manipCSV as manipCSV

import os, urllib, sys

import src.displayGraphs as dg
import src.AddCSVToOnto as AddCSVToOnto

CURRENT_DIRECTORY = os.getcwd()

BASE_DIR = os.path.dirname('mats/')

OntoSTEPNC = "http://www.semanticweb.org/ontologies/OntoSTEP-NC#"
AG_HOST = os.environ.get('AGRAPH_HOST', '192.168.57.129')
AG_PORT = int(os.environ.get('AGRAPH_PORT', '10035'))
AG_CATALOG = os.environ.get('AGRAPH_CATALOG', '')


AG_REPOSITORY = 'OntoMAP'


AG_USER = os.environ.get('AGRAPH_USER', 'test')
AG_PASSWORD = os.environ.get('AGRAPH_PASSWORD', 'xyzzy')

colList = ['NumBrut', 'NumOutil', 'Ligne_programme', 'Cycle_usinage', 'Temps_absolu', 'Temps_relatif', 'Vc', 'Fz', 'h',
           'AE', 'AP', 'AD', 'Diam_usine', 'Volume', 'Angle', 'Q', 'Temps_outil', 'OP', 'X', 'Y', 'Z', 'A', 'B', 'C',
           'X.1', 'Y.1', 'Z.1', 'I', 'J', 'K', 'S', 'TypeIntersect', 'TypeMovement', 'TypeInOut', 'ToolRef', 'Tool1',
           'Tool2', 'Dc', 'Lc', 'Rb', 'F', 'Void_41', 'Void_42', 'Void_43', 'Void_44', 'Void_45', 'prec_i_record',
           'prec_i_passant', 'ind_i_record', 'ind_i_passant']

classList = {}
relationList = {}


def stripResult(string):

    if isinstance(string, str):
        string = string[1:-1]
        return string.split('#')
    else:
        string = str(string)
        string = string[1:-1]
        return string.split('#')


def stripURI(string):
    print('string: ', string)
    # return string
    if '^^' in string:
        return string.split('"')[1]
    elif '/' in string.split('#')[1][:-1]:
        return string.split('#')[1][:-1].split('/')[-1]
    else:
        return string.split('#')[1][:-1]


class OntoClass:

    def __init__(self, uri):

        self.uri = uri
        self.isParent = {}
        self.isChild = {}
        self.isSuper = {}
        self.isSub = {}
        self.isExplored = [False, False, False, False, False]

    def getName(self):

        return stripResult(self.uri)[1]

    def createChildren(self):

        global relationList, classList
        if not self.isExplored[0]:
            queryString = """
            SELECT ?s ?p ?o
            WHERE{
                ?b owl:onClass ?s .
                ?o rdfs:subClassOf ?b .
                ?o rdf:type owl:Class .
                ?b owl:onProperty ?p .
                ?p rdf:type owl:ObjectProperty .
                filter (?s = %s)}""" % self.uri

            # print(queryString)
            # input()
            # print(queryString)
            tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString)
            result = tupleQuery.evaluate()

            for i in result:
                # print(i)
                tmpName = stripResult(i.getValue('p'))[1]
                relationList[tmpName] = relation(self.uri, i.getValue('o'), i.getValue('p'))
                if not tmpName in self.isParent.keys():
                    self.isParent[tmpName] = relationList[tmpName]
                objName = stripResult(i.getValue('o'))[1]
                # if not objName in classList.keys():
                #     classList[objName] = OntoClass(i.getValue('o'))
                #     classList[objName].isChild[tmpName] = relationList[tmpName]
                if not tmpName in classList[objName].isChild.keys():
                    classList[objName].isChild[tmpName] = relationList[tmpName]
            self.isExplored[0] = True

    def createParents(self):

        global relationList, classList
        if not self.isExplored[1]:
            # print('.', end='')
            queryString = """
            SELECT ?s ?p ?o
            WHERE{
                ?b owl:onClass ?s .
                ?o rdfs:subClassOf ?b .
                ?s rdf:type owl:Class .
                ?b owl:onProperty ?p .
                ?p rdf:type owl:ObjectProperty .
                filter (?o = %s)}""" % (self.uri)

            # print(queryString)
            # input()
            # print(queryString)
            tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString)
            result = tupleQuery.evaluate()

            for i in result:
                # print(i)
                tmpName = stripResult(i.getValue('p'))[1]
                if not tmpName in relationList.keys():
                    relationList[tmpName] = relation(i.getValue('s'), self.uri, i.getValue('p'))
                    if not tmpName in self.isChild.keys():
                        self.isChild[tmpName] = relationList[tmpName]
                    subjName = stripResult(i.getValue('s'))[1]
                    # if not subjName in classList.keys():
                    #     classList[subjName] = ontoClass(i.getValue('s'))
                    #     classList[subjName].isParent[tmpName] = relationList[tmpName]
                    if not tmpName in classList[subjName].isParent.keys():
                        classList[subjName].isParent[tmpName] = relationList[tmpName]
            queryString = """
            SELECT ?s ?p ?o
            WHERE{
                ?s ?p ?o .
                ?o rdf:type owl:Class .
                filter (?s = %s)
                filter (?p = stp:element_has_document_reference)}""" % (self.uri)
            tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString)
            result = tupleQuery.evaluate()
            for i in result:
                # print(i)
                tmpName = stripResult(i.getValue('p'))[1]
                if not tmpName in relationList.keys():
                    relationList[tmpName] = relation(i.getValue('s'), self.uri, i.getValue('p'))
                    if not tmpName in self.isChild.keys():
                        self.isChild[tmpName] = relationList[tmpName]
                    subjName = stripResult(i.getValue('s'))[1]
                    # if not subjName in classList.keys():
                    #     classList[subjName] = ontoClass(i.getValue('s'))
                    #     classList[subjName].isParent[tmpName] = relationList[tmpName]
                    if not tmpName in classList[subjName].isParent.keys():
                        classList[subjName].isParent[tmpName] = relationList[tmpName]
            self.isExplored[1] = True

    def createSuper(self):
        global classList
        if not self.isExplored[2]:
            # print('.', end='')
            queryString = """
            SELECT ?s
            WHERE{
                ?s rdfs:subClassOf ?o .
                ?s rdf:type owl:Class .
                filter (?o = %s)
            }""" % self.uri
            tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString)
            result = tupleQuery.evaluate()
            for i in result :
                subjName = stripResult(i.getValue('s'))[1]
                if subjName not in self.isSuper.keys():
                    # if not subjName in classList.keys():
                    #     classList[subjName] = ontoClass(i.getValue('s'))
                    self.isSuper[subjName] = classList[subjName]
                    classList[subjName].isSub[self.getName()] = classList[self.getName()]
            self.isExplored[2] = True

    def createSub(self):
        global classList
        if not self.isExplored[3]:
            # print('.', end='')
            queryString = """
            SELECT ?o
            WHERE{
                ?s rdfs:subClassOf ?o .
                ?o rdf:type owl:Class .
                filter
                ((?s = %s))
            }""" % (self.uri)
            tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString)
            result = tupleQuery.evaluate()
            for i in result :
                # print(stripResult(i.getValue('o')))
                objName = stripResult(i.getValue('o'))[1]
                if not objName in self.isSub.keys():
                    # if not objName in classList.keys():
                    #     classList[objName] = ontoClass(i.getValue('o'))
                    self.isSub[objName] = classList[objName]
                    classList[objName].isSuper[self.getName()] = classList[self.getName()]
            self.isExplored[3] = True

    def exploreNlevels(self, level):
        if level > self.isExplored[4]:
        # if level > self.isExplored[4]:
            global classList
            self.createChildren()
            self.createParents()
            self.createSub()
            self.createSuper()
            m = self.getName()
            if level > 1 :
                # if not self.getName() in ['assignment', 'comparison_expression']:
                for i in self.isParent:
                    # print(m)
                    # print(i)
                    classList[relationList[i].getChild()].exploreNlevels(level - 1)

                for i in self.isChild:
                    # print(m)
                    # print(i)
                    classList[relationList[i].getParent()].exploreNlevels(level - 1)

                for i in self.isSub:
                    # print(m)
                    classList[i].exploreNlevels(level - 1)

                for i in self.isSuper:
                    # print(m)
                    classList[i].exploreNlevels(level - 1)
            self.isExplored[4] = level

    def printDirectRelations(self):
        for i in self.isChild.values():
            print("{0:<20}, {1:<20}, {2:<20}".format(self.getName(), i.getName(), i.getParent()))
        for i in self.isParent.values():
            print("{0:<20}, {1:<20}, {2:<20}".format(i.getChild(), i.getName(), self.getName()))
        for i in self.isSuper:
            print("{0:<20}, rdfs:subClassOf     , {1:<20}".format(i, self.getName()))
        for i in self.isSub:
            print("\n\n{0:<20}, rdfs:subClassOf     , {1:<20}".format(self.getName(), i))
            print("{0} has relations :\n".format(i))
            self.isSub[i].printDirectRelations()
    # def printClass(self):
    #     filHandler =


class relation:
    def __init__(self, parent, child, name):
        self.parent = parent
        self.child = child
        self.name = name

    def getName(self):
        """Returns the stripped name of the relation"""
        return stripResult(self.name)[1]

    def getChild(self):
        """Returns the stripped name of the object"""
        return stripResult(self.child)[1]

    def getParent(self):
        """Returns the stripped name of the subject"""
        return stripResult(self.parent)[1]

    def printRelation(self):
        return ("{0} > {1} > {2}\n".format(self.getParent(), self.getName(), self.getChild()))


def getConnection(accessMode=Repository.ACCESS):
    """
    Tests getting the repository up.  Is called by the other examples to do the startup.
    """
    print("\nDefining connnection to AllegroGraph server -- host:'%s' port:%s" % (AG_HOST, AG_PORT))
    print("\nDefault working directory is '%s'" % (CURRENT_DIRECTORY))
    server = AllegroGraphServer(AG_HOST, AG_PORT, AG_USER, AG_PASSWORD)
    print("\nAvailable catalogs", server.listCatalogs())
    catalog = server.openCatalog(AG_CATALOG)  ## named catalog
    print("\nAvailable repositories in catalog '%s':  %s" % (catalog.getName(), catalog.listRepositories()))
    myRepository = catalog.getRepository(AG_REPOSITORY, accessMode)
    myRepository.initialize()
    conn = myRepository.getConnection()
    print("\nRepository %s is up!  It contains %i statements." % (myRepository.getDatabaseName(), conn.size()))
    conn.dropIndex("gospi")
    conn.dropIndex("gposi")
    conn.setNamespace('stp', 'http://www.semanticweb.org/ontologies/OntoSTEP-NC#')
    return conn


def getAllClasses():
    global classList
    queryString = """SELECT ?s  WHERE { ?s ?p ?o . filter ((?p = rdf:type) && (?o = owl:Class)) }"""
    tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, queryString)
    result = tupleQuery.evaluate()
    for i in result:
        tmpName = stripResult(i.getValue('s'))[1]
        if not tmpName in classList.keys():
            classList[tmpName] = OntoClass(i.getValue('s'))



# Ordre des relations dans OntoSTEP-NC :
# project > project_has_main_workplan > workplan
# workplan > workplan_has_elements > executable
# machining_workingstep is_a workingstep
# workingstep is_a executable
# machining_workingstep > machining_workingstep_has_operation > machining_operation
# machining_operation > machining_operation_has_technology > technology
# machining_operation > machining_operation_has_tool > machining_tool
# machining_tool > machining_tool_has_id > label
# technology > technology_has_feedrate > speed_measure


def createAllRelations():

    getAllClasses()

    for key in classList.keys():
        classList[key].exploreNlevels(2)


def getDictFromTxt(txtPath):

    myDict = {}
    with open(txtPath) as filHandler:
        for line in filHandler:
            q = line.replace('\n', '').split(':')
            myDict[q[0]] = q[1]
            myDict['projectName'] = txtPath.split('\\')[-1].split('.')[0]
    return myDict


def pathToSplit(pathList):

    global conn
    for i in pathList:
        if i.is_file():
            if i.path[-4:] == ".txt":
                for j in ['SAE', 'UF1', 'VTN', 'SPT', 'UTC']:
                    if j in i.path:
                        companyTag = j
                        print("Company tag : {}".format(companyTag))
                        break
                cnt = 1
                addInformations = getDictFromTxt(i.path)
                projectURI = AddCSVToOnto.addNewProject(conn, i.name.replace('.txt',''), addInformations, companyTag)
                print("Project name : {}".format(i.name.replace('.txt','')))
                if os.path.isfile(i.path.replace('.txt','.csv')):
                    csvList = manipCSV.splitBy(i.path.replace('.txt','.csv'))
                    prevWkstp = None
                    workplanURI = AddCSVToOnto.addNewWorkplan(conn, i.name.replace('.csv','').replace('.txt',''),
                                                              projectURI, companyTag)
                    for j in csvList:
                        parameters = manipCSV.tagAnalyseCoupe(j)
                        if prevWkstp: parameters['previous'] = prevWkstp
                        prevWkstp = AddCSVToOnto.addCSVtoOnto(parameters, workplanURI, conn, companyTag)
                else:
                    while os.path.isfile(i.path.replace('.txt', str(cnt) + '.csv')):
                        csvList = manipCSV.splitBy(i.path.replace('.txt', str(cnt) + '.csv'))
                        prevWkstp = None
                        workplanURI = AddCSVToOnto.addNewWorkplan(conn, i.path.replace('.txt', str(cnt)), projectURI, companyTag)
                        for j in csvList:
                            if str(j) == j:
                                parameters = manipCSV.tagAnalyseCoupe(j.replace('raw', 'spl'))
                            else:
                                parameters = manipCSV.tagAnalyseCoupe(j.path.replace('raw', 'spl'))
                            if prevWkstp: parameters['previous'] = prevWkstp
                            prevWkstp = AddCSVToOnto.addCSVtoOnto(parameters, workplanURI, conn, companyTag)
                        cnt += 1
        else:
            pathToSplit(os.scandir(i.path))  # TODO : Gérer création du nouveau dossier


def getVariables(SPARQLrequest):
    SPARQLrequest = SPARQLrequest.split('SELECT')[1].split('{')[0].split('FROM')[0].split('WHERE')[0]
    for i in ['DISTINCT', 'REDUCED']:
        if i in SPARQLrequest:
            SPARQLrequest = SPARQLrequest.split(i)[1]
    varList0 = SPARQLrequest.split(' ')
    varList = []
    for i in varList0:
        if len(i)!=0:
            varList.append(i[1:].rstrip())
    return varList

def runUpdateRequest(conn, request):
    print(request)
    updateQuery = conn.prepareUpdate(QueryLanguage.SPARQL, request)
    result = updateQuery.evaluate()
    return result

def printRequestResults(conn, request, count = False):
    tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, request)
    if not count :
        header = getVariables(request)
        # print(request)
        headerString = ''
        result = tupleQuery.evaluate()
        for i in header:
            headerString+="{:<35}|".format(i)
        print(headerString[:-1])
        # with tupleQuery.evaluate() as result :
        cnt = 0
        for i in result:
            # print(i)
            line = ''
            for j in i:
                # print(j)
                line += "{:<35}|".format(stripURI(str(j)))
            print(line[:-1])
            cnt += 1
    else :
        return tupleQuery.evaluate().rowCount()


def findPredicate(cl1, cl2, order = False):
    if cl2 in cl1.isParent.keys():  # <=> cl1 ?p cl2
        return cl1, cl1.isParent[cl2], cl2
    elif cl2 in cl1.isChild.keys() and not order:  # <=> cl2 ?p cl1
        return cl2, cl1.isChild[cl2], cl1
    elif cl2 in cl1.isSub.keys():  # <=> cl1 rdfs:subClassOf cl2
        return cl1, RDFS.SUBCLASSOF, cl2
    elif cl2 in cl1.isSuper.keys() and not order:  # <=> cl2 rdfs:subClassOf cl1
        return cl2, RDFS.SUBCLASSOF, cl1


def makeListItem(conn, request, myReq = ''):
    tupleQuery = conn.prepareTupleQuery(QueryLanguage.SPARQL, request)
    outputVar = getVariables(request)
    itemList = list()
    if len(outputVar) > 1:
        result = tupleQuery.evaluate()
        itemList = [[]]
        for i in result:
            toAdd = []
            for j in i:
                toAdd.append(stripURI(str(j)))
            itemList.append(toAdd)
    else:
        if myReq :
            result = tupleQuery.evaluate()
            for i in result:
                itemList.append([stripURI(str(i)), printRequestResults(conn, myReq%stripURI(str(i)), count = True)])
        else:
            result = tupleQuery.evaluate()
            for i in result:
                itemList.append(stripURI(str(i.getValue(outputVar[0]))))
    return itemList


def askWhichItem(conn, request, myReq = False):
    itemList = makeListItem(conn, request, myReq = myReq)
    cnt = 0
    if myReq:
        for i in itemList:
            print("{0:<5} : {1:>30} | {2}".format(cnt, itemList[cnt][0], itemList[cnt][1]))
            cnt += 1
    else:
        if len(itemList[0][0])>1:
            for i in itemList:
                print("{0:<5} : {1:>30}".format(cnt, itemList[cnt][0]))
                cnt += 1
        else:
            for i in itemList:
                print("{0:<5} : {1:>30}".format(cnt, itemList[cnt]))
                cnt += 1
            return itemList[int(input("Select the item :\n"))]
    return itemList[int(input("Select the item :\n"))][0]


def chooseFrom(myList):
    print("Choose 2 or 3 from the following list\nFormat X, Y, Z = '2, 3, 8'\n")
    cnt = 0
    isValid = False
    for i in myList:
        print("{0:<5} : {1:>30}".format(cnt, i))
        cnt+=1
    while not isValid:
        choice = input('Select the items(s) : \n')
        if len(choice.split(',')) in [2,3] :
            isValid = True
        else:
            print("Invalid set of items.\nChoose 2 or 3 from the previous list\nFormat X, Y, Z = '2, 3, 8'\n")
    chosenItems = []
    for j in choice.split(','):
        chosenItems.append(myList[int(j)])
    return chosenItems


def menu():
    global conn
    conn = getConnection()
    with open('../mats/getToolList.sparql') as filHandler:
        getToolList = filHandler.read()

    with open('../mats/feedFromTool.sparql') as filHandler:
        myFeedFromTool = filHandler.read()

    with open('../mats/request.sparql') as filHandler:
        addressFromTool = filHandler.read()

    #########################
    # # # # # # # # # # # # #
    #########################
    #  #  #  #  #  #  #  #  #
    #########################
    # # # # # # # # # # # # #
    #########################

    while 1:
        print("""\n\nFonctionnalités :
        00-\tGestion du format de données
        01-\tSélection d'outil + affichage des couples feed / spindle utilisés avec cet outil.
        02-\tAfficher tous les couples feed / spindle avec noms des outils en étiquette.
        03-\tAffichage des AP/AE pour un outil donné, projet donné en légende.
        04-\tAffichage de tous les outils un par un [ATTENTION C'EST LONG].
        05-\tAffichage des documents liés à l'utilisation d'un outil.
        06-\tAffichage des relations d'une entité.
        07-\tAfficher les points de passage d'un outil, par opération.
        99\tQuitter.\n\n""")
        choice = input("Fonctionnalité souhaitée : ")

        if choice == '1':
            # Bloc1 Sélection d'outil + affichage des couples feed / spindle utilisés avec cet outil
            toolName = askWhichItem(conn, getToolList, myReq = addressFromTool)
            feedRatesDict = {}
            feedRatesDict[toolName] = makeListItem(conn, myFeedFromTool%toolName)

            dg.scatterDisplay(feedRatesDict, getVariables(myFeedFromTool), annotation = True)

        elif choice == '2':
            # Bloc2 Afficher tous les couples feed / spindle avec noms des outils en étiquette
            toolList = makeListItem(conn, getToolList)
            feedRatesDict = {}
            for i in toolList:
                feedRatesDict[i] = makeListItem(conn, myFeedFromTool%i)

            dg.scatterDisplay(feedRatesDict, getVariables(myFeedFromTool), annotation = True)

        elif choice == '3':
            # Bloc3 Affichage des AP/AE pour un outil donné, projet donné en légende
            toolName = askWhichItem(conn, getToolList, myReq = addressFromTool)
            addressList = makeListItem(conn, addressFromTool%toolName)
            print(addressList)

            dg.displayFromCSVList(addressList, toolName, ('AP', 'AE'))

        elif choice == '4':
            # Afficher les AP/AE de tous les outils, un par un
            toolList = makeListItem(conn, getToolList, myReq = addressFromTool)
            for i in toolList:
                print(i)
                addressList = makeListItem(conn, addressFromTool%i[0])
                dg.displayFromCSVList(addressList, i[0], ('AP', 'AE'))

        elif choice == '5':
            toolName = askWhichItem(conn, getToolList, myReq = addressFromTool)
            printRequestResults(conn, addressFromTool%toolName)

        elif choice == '6':
            # Choisir entité
            createAllRelations()
            className = askWhichItem(conn, """SELECT ?s  WHERE { ?s rdf:type owl:Class .}""")
            ### Affichage des différents liens
            classList[className].printDirectRelations()

        elif choice == '7':
            # Choisir outil
            toolName = askWhichItem(conn, getToolList, myReq = addressFromTool)
            # Récupération adresses
            addressList = makeListItem(conn, addressFromTool%toolName)
            # addressList = [askWhichItem(conn, addressFromTool%toolName)]
            # Affichage
            toDispList = chooseFrom(colList)
            dg.displayFromCSVList(addressList, toolName, toDispList)

        elif choice == '99':
            quit()

        elif choice == '0':
            print("""\n\n[[ ADMINISTRATEURS ]]
        Gestion du format.
        1-\tCréation des liens 'realParent' pour l'affichage de Cong
        2-\tChargement des données issues des CSV découpés""".format(manipCSV.baseWrite))
            choice = input("Fonctionnalité souhaitée : ")
            if choice == '1':
                # Création des liens "realParent" pour affichage avec le travail de Cong :
                isRealParent = conn.createURI(namespace = OntoSTEPNC, localname = 'realParent')
                for key in sorted(relationList.keys()):
                    conn.add(relationList[key].child, isRealParent, relationList[key].parent)

            elif choice == '2':
                # Charge les fichiers csv contenus dans le répertoire manipCSV.baseWrite
                # /!\ Il faut avoir créé la base onto avant.
                print(manipCSV.tagAnalyseCoupe.__doc__)
                cnt = 0
                dirList = []
                for i in os.scandir('mats\\raw_csv'):
                    print("{0:<2} : {1:<3}".format(cnt, i.path[-3:]))
                    dirList.append(i)
                    cnt += 1
                cwd = dirList[int(input("Select company :\n"))]
                if input("\nExploration des csv du dossier {0}.\nConfirmer l'action ? Y/N\n".format(cwd.path)) == 'Y':
                    pathToSplit(os.scandir(cwd))
                    # TODO : Use new funcs

            else:
                print("Fonctionnalité inexistante ou indisponible")

        else:
            print("Fonctionnalité inexistante ou indisponible")
