import os
import csv


def renameColumns(filPath):

    outFilPath = filPath.replace('VTN\\', 'VTN2\\')
    outFilPath = outFilPath.replace('SAE\\', 'SAE2\\')
    outFilPath = outFilPath.replace('UF1\\', 'UF12\\')
    outFilPath = outFilPath.replace('UTC\\', 'UTC2\\')
    outFilPath = outFilPath.replace('SPT\\', 'SPT2\\')
    if not os.path.exists('\\'.join(outFilPath.split('\\')[:-1])):
        os.makedirs('\\'.join(outFilPath.split('\\')[:-1]))
    with open(filPath, 'r') as filHandler, open(outFilPath, 'w', newline='') as outHandler:
        r = csv.reader(filHandler, delimiter = ";")
        w = csv.writer(outHandler, delimiter = ';')

        next(r, None)
        w.writerow(['NumBrut', 'NumOutil', 'Ligne_programme', 'Cycle_usinage', 'Temps_absolu', 'Temps_relatif', 'Vc', 'Fz', 'h', 'AE', 'AP', 'AD', 'Diam_usine', 'Volume', 'Angle', 'Q', 'Temps_outil', 'OP', 'X','Y', 'Z', 'A', 'B', 'C', 'X.1', 'Y.1', 'Z.1', 'I', 'J', 'K', 'S', 'TypeIntersect', 'TypeMovement', 'TypeInOut', 'ToolRef', 'Tool1', 'Tool2', 'Dc', 'Lc', 'Rb', 'F', 'Void_41', 'Void_42', 'Void_43', 'Void_44', 'Void_45', 'prec_i_record', 'prec_i_passant', 'ind_i_record', 'ind_i_passant'])

        for row in r:
            w.writerow(row)


baseWrite = '..\mats\\raw_csv'

# for i in os.scandir(baseWrite):
#     print(i)
#     renameColumns(baseWrite + i)


def seek_and_rename(pathList):

    global conn
    for i in pathList:
        if i.is_file():
            if i.path[-4:] == ".csv":
                renameColumns(i.path)
        else:
            seek_and_rename(os.scandir(i.path))

seek_and_rename(os.scandir(baseWrite))




