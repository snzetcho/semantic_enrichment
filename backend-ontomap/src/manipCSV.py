import csv
import os.path
import re

baseWrite = 'mats/spl_csv/VTN/'

def streamlineString(string):
    nwStr = ''
    regex = re.compile('[a-zA-Z0-9_]')
    string = string.replace(' ', '_').replace('Ø','DIA')
    for i in string:
        if regex.match(i):
            nwStr += i
    return nwStr

def splitBy(filePath: object) -> object:
    pathList = []
    toWriteDir = os.path.split(filePath.replace('raw', 'spl'))[0]
    if not os.path.isdir(toWriteDir):
        os.mkdir(toWriteDir)
    with open(filePath) as csvFile :
        reader = csv.DictReader(csvFile)
        prevDisc = {}
        row = next(reader)
        for i in ["ToolRef", "Cycle_usinage"]:
            prevDisc[i] = ''
        for row in reader:
            for i in prevDisc.keys():
                if row[i] != prevDisc[i]:
                    flagToChange = True
                    prevDisc[i] = row[i]
            if flagToChange:
                try:
                    filHandler.close()
                except:
                    pass
                toWritePath = filePath.replace('raw', 'spl').replace('.csv', '')
                for i in prevDisc:
                    toWritePath += streamlineString(prevDisc[i])
                if os.path.isfile(toWritePath + '.csv'):
                    cnt = 0
                    while os.path.isfile(toWritePath + str(cnt) + '.csv'):
                        cnt += 1
                    toWritePath += str(cnt)
                toWritePath += '.csv'
                pathList.append(toWritePath)
                filHandler = open(toWritePath, "w")
                filWriter = csv.DictWriter(filHandler, reader.fieldnames)
                filWriter.writeheader()
                flagToChange = False
            filWriter.writerow(row)
    return pathList


def tagAnalyseCoupe(filePath):
    """Cette fonction sert uniquement pour les fichiers CSV d'analyse de coupe
    tels qu'importés en date du 11/01/2018.
    On considère, de base, un _document_ relié à une workingstep.
    On extrait, de ce _document_ :
    >> Un nom/adresse de fichier
    >> Un outil (Référence)
    >> Un nom d'opération
    >> Un temps d'opération
    >> Un feed programmé
    >> Une spindle speed programmée
    >> Un AE max
    >> Un AP max
    """
    with open(filePath) as myCSVFile:
        reader = csv.DictReader(myCSVFile)
        dictParam = {
        "address" : filePath,
        "toolRef" : '',
        "opName" : '',
        "opRunTime" : 0,#
        "feedRate" : '',
        "spindle" : ''
        }
        row = next(reader)
        dictParam['toolRef'] = streamlineString(row['ToolRef'])
        dictParam['opName'] = streamlineString(row["Cycle_usinage"])
        dictParam['feedRate'] = row["F"]
        dictParam['spindle'] = row["S"]
    return dictParam

def pathToSplit(entry):
    if entry.is_file():
        if entry.stat.st_type == "csv":
            splitBy(entry.path)

        elif entry.stat.st_type == "txt":
            pass
    else:
        pathToSplit(entry.path)


# Ordre des relations dans OntoSTEP-NC :
# project > project_has_main_workplan > workplan
# workplan > workplan_has_elements > executable
# machining_workingstep rdfs:subClassOf workingstep
# workingstep rdfs:subClassOf executable
# machining_workingstep > machining_workingstep_has_operation > machining_operation
# machining_operation > machining_operation_has_technology > technology
# machining_operation > machining_operation_has_tool > machining_tool
# technology > technology_has_feedrate > speed_measure

# print(tagAnalyseCoupe("mats\spl_csv\VTN\\ventana_3950043701FS6ENLEVEMENTDEMATIEREEXTREMITE.csv"))


# dfqsg
