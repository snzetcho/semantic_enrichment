//on va chercgher les différents elment de notre page
const pages=document.querySelectorAll(".page")
const header=document.querySelectorAll("header")
const nbPages=pages.length  //nbr de pages du formulaire
let pageActive = 1

//on attend le chargement de la page
window.onload=() => {
    // on affiche la 1ere page du formulaire
    document.querySelector(".page").style.display = "initial"
    // afficher les numéro de page dans l'entête
    // on parcourt la liste des pages
    pages.forEach((page, index) => {
        // on crée un elemtn qui est le rond avec le numéro
        let element = document.createElement("div")
        element.classList.add("page-num")
        element.id="num"+(index+1)
        if(pageActive == index+1){
            element.classList.add("active")
        }
        element.innerHTML=index+1
        header.appendChild(element)
    })
    //on gère les boutons suivant mtn
    let boutons=document.querySelectorAll("button[type=button]")
    for(let bouton of boutons){
        bouton.addEventListener("click", pageSuivante)
    }
}

// avancement du formulair d'une page
function pageSuivante(){
        //on masque toutes les pages
    for(let page of pages){
        page.style.display="none"
    }
    //on affiche la page suivante
    this.parentElement.nextElementSibling.style.display="initial"
    // on désactive la page active
    document.querySelector(".active").classList.remove("active")

    //on incrément page active
    pageActive++

    // on active la nouvelle page
    document.querySelector("#num"+pageActive).classList.add("active")


}